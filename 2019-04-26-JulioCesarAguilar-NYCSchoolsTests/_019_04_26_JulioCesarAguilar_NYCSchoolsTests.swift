//
//  _019_04_26_JulioCesarAguilar_NYCSchoolsTests.swift
//  2019-04-26-JulioCesarAguilar-NYCSchoolsTests
//
//  Created by Julio Cesar Aguilar Jimenez on 26/04/2019.
//  Copyright © 2019 Julio C. Aguilar. All rights reserved.
//

import XCTest
@testable import _019_04_26_JulioCesarAguilar_NYCSchools

class _019_04_26_JulioCesarAguilar_NYCSchoolsTests: XCTestCase {
    
    func testLoadingSchoolsResponse() {
        // Expectation that will help to wait some time until the API can retrieve the information
        let expectation = self.expectation(description: "API Response Parse Expectation")
        
        let schools = SchoolsResource.init()
        
        Webservice().load(resource: schools.resource) { result in
            XCTAssertNotNil(result, "Should work, and retrieve data")
            guard let schoolVM: SchoolViewModel = result?.first else {
                XCTFail("Should parse the data into a SchoolViewmodel")
                return
            }
            XCTAssertNotNil(schoolVM.name, "Should parse indiviudal keys")
            XCTAssertFalse(schoolVM.name == "", "Should retrieve a school name")
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 8, handler: nil)
    }
    
    func testLoadingSchoolShouldFailed() {
        let expectation = self.expectation(description: "API Response Parse Expectation")
        
        let failedURL = "https://data.cityofnewyork.us/s3k6-pzi2.json"
        let schools = SchoolsResource(url: failedURL)
        
        Webservice().load(resource: schools.resource) { result in
            XCTAssertNil(result, "Shouldn't work, it must have nothing")
            if let schoolVM: SchoolViewModel = result?.first {
                XCTFail("Shouldn't parse the data into a SchoolViewmodel")
                return
            }
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testLoadingSchoolDetailResponse() {
        // Expectation that will help to wait some time until the API can retrieve the information
        let expectation = self.expectation(description: "API Response Parse Expectation")
        
        let validDBN = "21K728"
        let scores = ScoresResource.init(schoolDBN: validDBN)
        
        Webservice().load(resource: scores.resource) { result in
            XCTAssertNotNil(result, "Should work, and retrieve data")
            guard let scoresVM: ScoresViewModel = result?.first else {
                XCTFail("Should parse the data into a ScoresViewModel")
                return
            }
            XCTAssertNotNil(scoresVM.takers, "Should parse indiviudal keys")
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 8, handler: nil)
    }

    func testLoadingSchoolDetailShouldFailed() {
        // Expectation that will help to wait some time until the API can retrieve the information
        let expectation = self.expectation(description: "API Response Parse Expectation")
        
        let invalidDBN = "AAAAA"
        let scores = ScoresResource.init(schoolDBN: invalidDBN)
        
        Webservice().load(resource: scores.resource) { result in
            XCTAssertNotNil(result, "Should work and retrieve something empty")
            
            guard let _ = result?.first else {
                expectation.fulfill()
                return
            }
            XCTFail("It shouldn't be anything inside result")
        }
        self.waitForExpectations(timeout: 5, handler: nil)
    }
}
