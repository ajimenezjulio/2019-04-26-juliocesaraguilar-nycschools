//
//  ScoresViewModel.swift
//  2019-04-26-JulioCesarAguilar-NYCSchools
//
//  Created by Julio Cesar Aguilar Jimenez on 26/04/2019.
//  Copyright © 2019 Julio C. Aguilar. All rights reserved.
//

import Foundation

// Model for the SAT scores of a school
struct ScoresViewModel: Decodable {
    let takers: String
    let reading: String
    let math: String
    let writing: String
    
    // Personalise the decoding keys
    private enum CodingKeys: String, CodingKey {
        case takers = "num_of_sat_test_takers"
        case reading = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
        case writing = "sat_writing_avg_score"
    }
}
