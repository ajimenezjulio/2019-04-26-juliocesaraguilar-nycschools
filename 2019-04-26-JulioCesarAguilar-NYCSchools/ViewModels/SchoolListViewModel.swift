//
//  SchoolListViewModel.swift
//  2019-04-26-JulioCesarAguilar-NYCSchools
//
//  Created by Julio Cesar Aguilar Jimenez on 26/04/2019.
//  Copyright © 2019 Julio C. Aguilar. All rights reserved.
//

import Foundation
import UIKit

// Model that will keep all the NYC schools
class SchoolListViewModel {
    private(set) var schoolViewModels = [SchoolViewModel]()
    
    // Set the schoolViewModels
    func setSchoolViewModels(with viewModels: [SchoolViewModel]) {
        self.schoolViewModels = viewModels
    }
}


// Model for a single school, it implements the decodable protocol for the parsing process
struct SchoolViewModel: Decodable {
    let name: String
    let interest: String
    let city: String
    let state: String
    let dbn: String
    let overview: String
    var logo: UIImage?
    
    // Personalise the decoding keys and tell the protocol that logo isn't part of the JSON
    private enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case interest = "interest1"
        case city
        case state = "state_code"
        case dbn
        case overview = "overview_paragraph"
    }
}
