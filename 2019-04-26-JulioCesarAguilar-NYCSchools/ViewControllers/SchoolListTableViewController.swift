//
//  SchoolListTableViewController.swift
//  2019-04-26-JulioCesarAguilar-NYCSchools
//
//  Created by Julio Cesar Aguilar Jimenez on 26/04/2019.
//  Copyright © 2019 Julio C. Aguilar. All rights reserved.
//

import UIKit

class SchoolListTableViewController: UITableViewController, ActivityIndicatorProtocol {
    
    private var schoolListViewModel = SchoolListViewModel()
    private var datasource: TableViewDataSource<SchoolTableViewCell, SchoolViewModel>!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Enabling the large title feature
        self.navigationController?.navigationBar.prefersLargeTitles = true
        // Building our datasource
        self.datasource = TableViewDataSource(cellIdentifier: kSCHOOLCELL, items: self.schoolListViewModel.schoolViewModels) { cell, vm in
            cell.configure(vm)
        }
        // Telling the TableViewController who is going to be the datasource
        self.tableView.dataSource = self.datasource
        // Load the NYC schools
        loadSchools()
    }
    
    // Animation for the table view
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateTable(tableView: self.tableView)
    }
    
    
    // MARK: TableView Delegate Functions
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Create the school detail VC
        let detailVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "schoolDetail") as! SchoolDetailViewController
        // Pass school to chatVC
        detailVC.schoolViewModel = self.schoolListViewModel.schoolViewModels[indexPath.row]
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    
    // MARK: Load Data
    func loadSchools() {
        // Start animation and keeping the view references to remove them at the end and avoid mempry leaks
        let viewReferences: [UIView]
        // We need the height of the navigation bar to adjust the activity at the half
        viewReferences = self.showActivityIndicator(navigationHeight: (self.navigationController?.navigationBar.frame.height)!)
        
        let schools = SchoolsResource.init()
        // Load the schools and notify the datasource and tableview to display the data when finished
        Webservice().load(resource: schools.resource) { [weak self] result in
            guard let schoolsVM = result else {
                self?.hideActivityIndicator(viewsToDismiss: viewReferences)
                self?.navigationController?.show(message: "We're sorry, there's no data available. Please try later")
                return
            }
            self?.configureUI(schoolsVM: schoolsVM)
            // Stop activity
            self?.hideActivityIndicator(viewsToDismiss: viewReferences)
        }
    }
    
    
    // MARK: UISetup
    func configureUI(schoolsVM: [SchoolViewModel]){
        self.schoolListViewModel.setSchoolViewModels(with: schoolsVM)
        self.datasource.updateItems((self.schoolListViewModel.schoolViewModels))
        // Reload and animate the tableview
        animateTable(tableView: self.tableView)
    }
    
    
    // MARK: IBActions
    @IBAction func didRefreshTapped(_ sender: Any) {
        loadSchools()
    }
    
}
